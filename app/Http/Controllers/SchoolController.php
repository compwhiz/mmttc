<?php

namespace App\Http\Controllers;

use App\School;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('setup.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request, School $school)
    {
        $this->validate($request, [
            'name'           => ['required'],
            'knec'           => ['required'],
            'tsc'            => ['required'],
            'moecode'        => ['required'],
            'mission'        => ['required'],
            'vission'        => ['required'],
            'motto'          => ['required'],
            'location'       => ['required'],
            'address'        => ['required'],
            'telephone'      => ['required'],
            'status'         => ['required'],
            'enrollmenttype' => ['required'],
            'category'       => ['required'],
            'mobile'         => ['required'],
            'website'        => ['required'],
            'email'          => ['required'],
            'logo'           => ['required'],
        ]);

        $school->school = $request->name;
        $school->kneccode = $request->knec;
        $school->tsccode = $request->tsc;
        $school->moecode = $request->moecode;
        $school->mission = $request->mission;
        $school->vission = $request->vission;
        $school->motto = $request->motto;
        $school->location = $request->location;
        $school->address = $request->address;
        $school->telephone = $request->telephone;
        $school->status = $request->status;
        $school->enrollmentype = $request->enrollmenttype;
        $school->category = $request->category;
        $school->mobile = $request->mobile;
        $school->website = $request->website;
        $school->email = $request->email;
        $school->logo = $request->logo;
        $school->saveOrFail();
        return response()->json($school);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\School $school
     * @return \Illuminate\Http\Response
     */
    public function show(School $school)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\School $school
     * @return \Illuminate\Http\Response
     */
    public function edit(School $school)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\School $school
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, School $school)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\School $school
     * @return \Illuminate\Http\Response
     */
    public function destroy(School $school)
    {
        //
    }
}
