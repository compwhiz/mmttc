<?php

namespace App\Http\Controllers;

use App\Subjectcombination;
use Illuminate\Http\Request;

class SubjectcombinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('setup.subjectcombination');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request, Subjectcombination $combination)
    {
        $this->validate($request, [
            'code'            => ['required'],
            'combinationname' => ['required'],
            'isdefault'       => ['required'],
        ]);

        $combination->code = $request->code;
        $combination->combinationname = $request->combinationname;
        $combination->isdefault = $request->isdefault;
        $combination->saveOrFail();
        return response()->json($combination);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subjectcombination $subjectcombination
     * @return \Illuminate\Http\Response
     */
    public function show(Subjectcombination $subjectcombination)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subjectcombination $subjectcombination
     * @return \Illuminate\Http\Response
     */
    public function edit(Subjectcombination $subjectcombination)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Subjectcombination $subjectcombination
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subjectcombination $subjectcombination)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subjectcombination $subjectcombination
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subjectcombination $subjectcombination)
    {
        //
    }
}
