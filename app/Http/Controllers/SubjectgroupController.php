<?php

namespace App\Http\Controllers;

use App\Subjectgroup;
use Illuminate\Http\Request;

class SubjectgroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('setup.subjectgroup');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request,Subjectgroup $group)
    {
        $this->validate($request, [
            'category'     => ['required'],
            'categoryname' => ['required'],
            'sortposition' => ['required'],
        ]);
        $group->category = $request->category;
        $group->name = $request->categoryname;
        $group->sortposition = $request->sortposition;
        $group->saveOrFail();
        return response()->json($group);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subjectgroup $subjectgroup
     * @return \Illuminate\Http\Response
     */
    public function show(Subjectgroup $subjectgroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subjectgroup $subjectgroup
     * @return \Illuminate\Http\Response
     */
    public function edit(Subjectgroup $subjectgroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Subjectgroup $subjectgroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subjectgroup $subjectgroup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subjectgroup $subjectgroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subjectgroup $subjectgroup)
    {
        //
    }
}
