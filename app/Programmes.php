<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programmes extends Model
{
    public function course()
    {
        return $this->hasOne(Course::class, 'id','course');
    }
}
