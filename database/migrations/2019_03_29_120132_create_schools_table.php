<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('school');
            $table->string('logo');
            $table->string('regcode')->nullable();
            $table->string('moecode')->nullable();
            $table->string('tsccode')->nullable();
            $table->string('kneccode')->nullable();
            $table->longText('mission')->nullable();
            $table->longText('vission')->nullable();
            $table->longText('motto')->nullable();
            $table->longText('location')->nullable();
            $table->string('address')->nullable();
            $table->string('telephone')->nullable();
            $table->string('status')->nullable();
            $table->string('enrollmentype')->nullable();
            $table->string('category')->nullable();
            $table->string('mobile')->nullable();
            $table->string('website')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
