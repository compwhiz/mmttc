<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgrammesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programmes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('coursename')->nullable();
            $table->string('department')->nullable();
            $table->string('gradetype')->nullable();
            $table->string('cert')->nullable();
            $table->string('period')->nullable();
            $table->string('year')->nullable();
            $table->string('closed')->nullable();
            $table->string('maxunits')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programmes');
    }
}
