<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('course');
            $table->string('fullname');
            $table->string('Address')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('nationalID')->nullable();
            $table->string('dob')->nullable();
            $table->string('sex')->nullable();
            $table->string('passport')->nullable();
            $table->string('kcsecert')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
