<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addboarder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->string('name');
            $table->string('idnumber')->nullable();
            $table->string('email')->nullable();
            $table->string('bithdate')->nullable();
            $table->string('gender')->nullable();
            $table->string('county')->nullable();
            $table->string('address')->nullable();
            $table->string('religion')->nullable();
            $table->string('regnumber')->nullable();
            $table->string('programme')->nullable();
            $table->boolean('border')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            //
        });
    }
}
