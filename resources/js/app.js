/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
// var router = require('vue-router');
//
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('schoolinfo', require('./components/ExampleComponent.vue').default);
Vue.component('subject', require('./components/setup/subject.vue').default);
Vue.component('subjectgroup', require('./components/setup/subjectgroup.vue').default);
Vue.component('subjectcombination', require('./components/setup/subjectcombination.vue').default);
Vue.component('application', require('./components/application/application').default);
Vue.component('showapplications', require('./components/application/show').default);
Vue.component('tracking', require('./components/application/tracking').default);
// Vue.component('subject', require('./components/setup/subject'));


Vue.component('studentreg', require('./components/students/create').default);
Vue.component('students', require('./components/students/index').default);
Vue.component('programmereg', require('./components/programme/index').default);
Vue.component('programmes', require('./components/programme/all').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
// Vue.use(router);
window.Bus = new Vue({});
const app = new Vue({
    el: '#app'
});
const front = new Vue({
    el: '#front'
});
