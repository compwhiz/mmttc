@extends('layouts.home')

@section('content')

    <div class="theme-inner-banner">
        <div class="opacity">
            <ul>
                <li><a href="">Home</a></li>
                <li>/</li>
                <li>About</li>
            </ul>
            <h2>About Us</h2>
        </div> <!-- /.opacity -->
    </div>
    <div class="our-history section-margin-top">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-xs-12">
                    <div class="text">
                        <h2>MMTTC's background</h2>
                        <p>Machakos Medical &amp; Technical Training College is a private facility approved by the ministry of
                            education and offers a variety of courses. The courses selected supplement the current market
                            demand and give opportunities to students unable to secure admissions in the limited number of
                            both public and other private institutions. The programs are at various levels; short courses,
                            certificate and diploma and they give academic progression, professional development and
                            technological skills. Currently the courses on offer are in the fields of medical professions, social
                            sciences, business administration and information technology. The programs employ modern
                            technologies designed to meet the needs of the nation and the rest of the world including
                            consultancy services.</p>

                    </div> <!-- /.text -->
                </div> <!-- /.col- -->
                <div class="col-md-5 col-xs-12"><img src="{{ asset('front/images/home/7.jpg') }}" alt=""></div>
            </div> <!-- /.row -->

            <div class="row our-goal">
                <div class="col-sm-6 col-xs-12">
                    <h5>Our Mission</h5>
                    <p>Our mission is to provide a setting for the development of intellect, skills, attitudes and values
                        and produce experts in various fields in line with the current job market demands and contribute
                        towards economic growth.</p>
{{--                    <a href="#" class="read-more">Read More <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>--}}
                </div>
                <div class="col-sm-6 col-xs-12">
                    <h5>Our Vision</h5>
                    <p>Our vision is to become a well-established and reputable institution in the provision of quality
                        professional training.</p>
{{--                    <a href="#" class="read-more">Read More <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>--}}
                </div>
                <div class="col-sm-6 col-xs-12">
                    <h5>Our Motto</h5>
                    <p>Knowledge is power</p>
                    {{--                    <a href="#" class="read-more">Read More <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>--}}
                </div>
            </div> <!-- /.our-goal -->

            <div class="row our-history our-goal">
               <div class="col-md-12">
                   <h5 style="padding: 5px !important;">Our Values</h5>
                   <div class="text">
                       <ul class="clearfix">
                           <li><i class="flaticon-book2"></i> <h6>Quality services</h6></li>
                           <li><i class="flaticon-book3"> </i><h6>Integrity</h6></li>
                       </ul>
                       <ul class="clearfix">
                           <li><i class="flaticon-book3"> </i><h6>Accountability &amp; transparency</h6></li>
                           <li><i class="flaticon-book3"> </i><h6>Innovation and adaptation</h6></li>
                       </ul>
                       <ul class="clearfix">
                           <li><i class="flaticon-book3"> </i><h6>empowerment</h6></li>

                       </ul>
                   </div>
               </div>
            </div>

            <div class="our-analytics">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="analytics-chart">
                            <div class="piechart"  data-border-color="#6664d4" data-value=".77">
                                <span>.77</span>
                            </div>
                            <h5>Happy Students</h5>
                        </div> <!-- /.analytics-chart -->
                    </div> <!-- /.col- -->
                    <div class="col-xs-4">
                        <div class="analytics-chart">
                            <div class="piechart"  data-border-color="#6664d4" data-value=".99">
                                <span>.99</span>
                            </div>
                            <h5>Success in Courses</h5>
                        </div> <!-- /.analytics-chart -->
                    </div> <!-- /.col- -->
                    <div class="col-xs-4">
                        <div class="analytics-chart">
                            <div class="piechart"  data-border-color="#6664d4" data-value=".90">
                                <span>.90</span>
                            </div>
                            <h5>Graduates Students</h5>
                        </div> <!-- /.analytics-chart -->
                    </div> <!-- /.col- -->
                </div> <!-- /.row -->
            </div> <!-- /.our-analytics -->
        </div> <!-- /.container -->
    </div>

    <div class="short-banner-two bg-two">
        <div class="opacity color-two">
            <div class="container">
                <h6>We offer quality education at friedly prices</h6>
                <a href="#" class="wow fadeInLeft animated theme-solid-button">Brochure</a>
                <a href="#" class="wow fadeInRight animated theme-line-button">Fee Structure</a>
            </div>
        </div>
    </div>
@endsection