@extends('layouts.home')

@section('content')

    <div class="theme-inner-banner">
        <div class="opacity">
            <ul>
                <li><a href="">Home</a></li>
                <li>/</li>
                <li>Admissions</li>
            </ul>
            <h2>Admissions</h2>
        </div> <!-- /.opacity -->
    </div>
    <div class="admissions">
        <div class="row">
            <div class="container application">
                <div class="col-md-12 p-4">
                    <div class="text">
                        <application :courses='@json(\App\Programmes::all())'></application>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection