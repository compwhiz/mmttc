@extends('layouts.home')

@section('content')

    <div class="theme-inner-banner">
        <div class="opacity">
            <ul>
                <li><a href="">Home</a></li>
                <li>/</li>
                <li>Track Application</li>
            </ul>
{{--            <h2>{{ $applicant->fullname }}</h2>--}}
        </div> <!-- /.opacity -->
    </div>
    <div class="our-history section-margin-top">
        <div class="container">
            <div class="row">
                <div class="col-md-4 hidden-sm">
                    <img src="{{asset('logo.JPG')}}" alt="">
                </div>
                <div class="col-md-8">
                    <tracking :applicant='@json($applicant)'></tracking>
                </div>
            </div>
<div>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <ul>
            <li>Ensure that the information is correct.</li>
            <li>Upload only scanned images of KCSE Certificate and passport.</li>
        </ul>
    </div>
</div>
        </div>
    </div>
    <div class="short-banner-two bg-two">
        <div class="opacity color-two">
            <div class="container">
                <h6>We offer quality education at friedly prices</h6>
                <a href="#" class="wow fadeInLeft animated theme-solid-button">Brochure</a>
                <a href="#" class="wow fadeInRight animated theme-line-button">Fee Structure</a>
            </div>
        </div>
    </div>
@endsection