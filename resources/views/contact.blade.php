@extends('layouts.home')

@section('content')
    <div class="theme-inner-banner">
        <div class="opacity">
            <ul>
                <li><a href="index.html">Home</a></li>
                <li>/</li>
                <li>Contact</li>
            </ul>
            <h2>Contact</h2>
        </div> <!-- /.opacity -->
    </div> <!-- /.theme-inner-banner -->



    <!--
    =============================================
        Contact Us Page
    ==============================================
    -->
    <div class="contact-page">
        <div class="container">
            <div class="google-map"><div class="map-canvas"></div></div>

            <div class="contact-form-wrapper">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="theme-form-style-one">
                            <h3>Request for Contact</h3>
                            <form action="http://themazine.com/html/remakri/inc/sendemail.php" class="form-validation" autocomplete="off">
                                <div class="single-input">
                                    <label>Your full name</label>
                                    <input type="text" placeholder="Type name" name="name">
                                </div> <!-- /.single-input -->
                                <div class="single-input">
                                    <label>Your email address</label>
                                    <input type="email" placeholder="Type email" name="email">
                                </div> <!-- /.single-input -->
                                <div class="single-input">
                                    <label>Short your personal message</label>
                                    <textarea placeholder="Type Message" name="message"></textarea>
                                </div> <!-- /.single-input -->
                                <input type="submit" value="Get request" class="theme-solid-button theme-button">
                            </form>
                        </div> <!-- /.theme-form-style-one -->
                    </div> <!-- /.col- -->
                    <div class="col-md-6 col-xs-12">
                        <div class="contact-address">
                            <ul class="address">
                                <li><i class="flaticon-map-bold"></i> Machakos</li>
                                <li><i class="flaticon-email"></i> info@mmttc.ac.ke</li>
                                <li><i class="flaticon-call"></i> 0722 383 285</li>
                            </ul>
                            <ul class="social-icon">
                                <li><a href="#" class="tran3s"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="tran3s"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="tran3s"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="tran3s"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="tran3s"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            </ul>
                        </div> <!-- /.contact-address -->
                    </div> <!-- /.col- -->
                </div> <!-- /.row -->
            </div> <!-- /.contact-form-wrapper -->
        </div> <!-- /.container -->

        <div class="contact-validation-markup">
            <!--Contact Form Validation Markup -->
            <!-- Contact alert -->
            <div class="alert-wrapper" id="alert-success">
                <div id="success">
                    <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <div class="wrapper">
                        <p>Your message was sent successfully.</p>
                    </div>
                </div>
            </div> <!-- End of .alert-wrapper -->
            <div class="alert-wrapper" id="alert-error">
                <div id="error">
                    <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <div class="wrapper">
                        <p>Sorry!Something Went Wrong.</p>
                    </div>
                </div>
            </div> <!-- End of .alert-wrapper -->
        </div> <!-- /.contact-validation-markup -->
    </div> <!-- /.contact-page -->



    <!--
    =============================================
        Newsletter Banner
    ==============================================
    -->
    <div class="newsletter-banner">
        <div class="container">
            <h5 class="float-left">Sign up for Subscribe Newsletter</h5>
            <form action="#" class="float-right">
                <input type="email" placeholder="Your Email">
                <button class="theme-solid-button theme-button">Submit</button>
            </form>
        </div> <!-- /.container -->
    </div> <!-- /.newsletter-banner -->


@endsection