@extends('layouts.home')

@section('content')
    <div id="theme-main-banner" class="banner-one section-margin-bottom">
        <div data-src="{{ asset('mmttc.jpg') }}">
            <div class="camera_caption">
                <div class="container">
                    <h4 class="wow fadeInUp animated">In the Kitchen</h4>
                    <h1 class="wow fadeInUp animated" data-wow-delay="0.2s">The catering team doing what they do
                        best.</h1>
                </div> <!-- /.container -->
            </div> <!-- /.camera_caption -->
        </div>
        <div data-src="{{ asset('banner/DSC04961.JPG') }}">
            <div class="camera_caption text-center">
                <div class="container">
                    <h4 class="wow fadeInUp animated">Engineering</h4>
                    <h1 class="wow fadeInUp animated" data-wow-delay="0.2s">We understand engineering</h1>
                </div> <!-- /.container -->
            </div> <!-- /.camera_caption -->
        </div>
        <div data-src="{{ asset('banner/DSC04116.JPG') }}">
            <div class="camera_caption text-center">
                <div class="container">
                    <h4 class="wow fadeInUp animated">MMTTC Choir</h4>
                    <h1 class="wow fadeInUp animated" data-wow-delay="0.2s">Putting God first.</h1>
                </div> <!-- /.container -->
            </div> <!-- /.camera_caption -->
        </div>
        <div data-src="{{ asset('banner/DSC04686.JPG') }}">
            <div class="camera_caption text-center">
                <div class="container">
                    <h4 class="wow fadeInUp animated">MMTTC Football Team</h4>
                    <h1 class="wow fadeInUp animated" data-wow-delay="0.2s">During a game</h1>
                </div> <!-- /.container -->
            </div> <!-- /.camera_caption -->
        </div>
        <div data-src="{{ asset('banner/DSC_1297.JPG') }}">
            <div class="camera_caption text-center">
                <div class="container">
                    <h4 class="wow fadeInUp animated">2018 Graduation</h4>
                    <h1 class="wow fadeInUp animated" data-wow-delay="0.2s">Students graduationg in 2018</h1>
                </div> <!-- /.container -->
            </div> <!-- /.camera_caption -->
        </div>
        <div data-src="{{ asset('banner/DSC_1587.JPG') }}">
            <div class="camera_caption text-center">
                <div class="container">
                    <h4 class="wow fadeInUp animated">Our principals Office</h4>
                    <h1 class="wow fadeInUp animated" data-wow-delay="0.2s">Our principal receiving the guest of
                        honor</h1>
                </div> <!-- /.container -->
            </div> <!-- /.camera_caption -->
        </div>
    </div> <!-- /#theme-main-banner -->
    <div class="our-feature section-margin-top">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12 float-right">
                    <h2>We will give you future</h2>
                    <div class="mark-text">We are well-established and reputable institution in the provision of quality
                        and professional training.
                    </div>
                    <p>Machakos Medical &amp; Technical Training College is a private facility approved by the ministry
                        of
                        education and offers a variety of courses. The courses selected supplement the current market
                        demand and give opportunities to students unable to secure admissions in the limited number of
                        both public and other private institutions.</p>
                    <a href="#" class="wow fadeInUp animated theme-solid-button theme-button">More about Us</a>
                </div>
                <div class="col-md-6 col-xs-12"><img src="{{ asset('front/images/home/shape1.png') }}" alt=""></div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.our-feature -->
    <div class="our-feature-course-one section-margin-top">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="feature-block">
                        <div class="wrapper">
                            <div class="icon"><i class="flaticon-exam"></i></div>
                            <h5><a href="">Library</a></h5>
                            {{--                            <p>This award is designed to give you a taster of teaching and prepare you to teach and will introduce </p>--}}
                        </div>
                    </div> <!-- /.feature-block -->
                </div> <!-- /.col- -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="feature-block">
                        <div class="wrapper">
                            <div class="icon"><i class="flaticon-book4"></i></div>
                            <h5><a href="">Markatable Courses</a></h5>
                            {{--                            <p>The new federal education law provides great opportunities for states to exercise </p>--}}
                        </div>
                    </div> <!-- /.feature-block -->
                </div> <!-- /.col- -->
                <div class="col-md-4 hidden-sm col-xs-12">
                    <div class="feature-block">
                        <div class="wrapper">
                            <div class="icon"><i class="flaticon-book3"></i></div>
                            <h5><a href="">Co-curriculor Oriented</a></h5>
                            {{--                            <p>Natural disasters such as wildfires and floods, along with other types of emergency situations </p>--}}
                        </div>
                    </div> <!-- /.feature-block -->
                </div> <!-- /.col- -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.our-feature-course-one -->
    <div class="course-search-form shape-style">
        <div class="container">
            <div class="title">
                <h2>ONLINE <span>COURSES</span> SEARCH</h2>
                {{--                <p>Get more online courses, 4 reasons why you’ll love education </p>--}}
            </div> <!-- /.title -->
            <form action="#" class="course-form">
                <input type="text" placeholder="Your Courses Name">
                <button>Search Course</button>
            </form>
        </div> <!-- /.container -->
    </div>
    <div class="our-courses section-margin-top">
        <div class="container">
            <div class="theme-title-one theme-title">
                <h2>Our Courses</h2>
                <div class="icon"><i class="flaticon-book2"></i></div>
                <p>Select one of the courses to enroll</p>
            </div> <!-- /.theme-title-one -->

            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-course-block">
                        <div class="image"><img src="{{ asset('front/images/course/1.jpg') }}" alt=""></div>
                        <div class="text-box">
                            <h5><i class="flaticon-device"></i><a href="">Medical Courses</a></h5>

                        </div> <!-- /.text-box -->
                    </div> <!-- /.single-course-block -->
                </div> <!-- /.col- -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-course-block">
                        <div class="image"><img src="{{ asset('front/images/course/2.jpg') }}" alt=""></div>
                        <div class="text-box">
                            <h5><i class="flaticon-bulb"></i><a href="">Technical Courses</a></h5>
                        </div> <!-- /.text-box -->
                    </div> <!-- /.single-course-block -->
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-course-block">
                        <div class="image"><img src="{{ asset('front/images/course/2.jpg') }}" alt=""></div>
                        <div class="text-box">
                            <h5><i class="flaticon-bulb"></i><a href="">Business Courses</a></h5>
                        </div> <!-- /.text-box -->
                    </div> <!-- /.single-course-block -->
                </div> <!-- /.col- -->
                <!-- /.col- -->
            </div> <!-- /.row -->
            <div class="text-center show-more"><a href="#"
                                                  class="wow fadeInUp animated theme-solid-button theme-button">Show
                    More Courses</a></div>
        </div> <!-- /.container -->
    </div>
    <div class="short-banner">
        <div class="container">
            <div class="main-bg-wrapper">
                <div class="opacity clearfix">
                    <h4 class="float-left">April/May Intake Ongoing!</h4>
                    <a href="#" class="float-right theme-line-button theme-button">Apply Now !</a>
                </div> <!-- /.opacity -->
            </div> <!-- /.main-bg-wrapper -->
        </div> <!-- /.container -->
    </div>
    <div class="our-gallery section-margin-top">
        <div class="theme-title-one theme-title">
            <h2>Our Gallery</h2>
            <div class="icon"><i class="flaticon-exam"></i></div>
            {{--            <p>Our gallery</p>--}}
        </div> <!-- /.theme-title-one -->
        <div class="gallery-wrapper box-layout">
            <div class="row">
                <div class="col-md-3 col-xs-6">
                    <div class="gallery-image-wrapper">
                        <div class="image">
                            <img src="{{ asset('DSC04115.JPG') }}" alt="">
                            <div class="opacity"><a data-fancybox="project" href="{{ asset('DSC04115.JPG') }}"
                                                    class="zoom-view" title="We’ve done lot’s of work, Let’s Check"><i
                                            class="fa fa-search" aria-hidden="true"></i></a></div>
                        </div>
                    </div> <!-- /.gallery-image-wrapper -->
                </div> <!-- /.col- -->
                <div class="col-md-3 col-xs-6">
                    <div class="gallery-image-wrapper">
                        <div class="image">
                            <img src="{{ asset('mmttc.jpg') }}" alt="">
                            <div class="opacity"><a data-fancybox="project" href="{{ asset('mmttc.jpg') }}"
                                                    class="zoom-view" title="We’ve done lot’s of work, Let’s Check"><i
                                            class="fa fa-search" aria-hidden="true"></i></a></div>
                        </div>
                    </div> <!-- /.gallery-image-wrapper -->
                </div> <!-- /.col- -->
                <div class="col-md-3 col-xs-6">
                    <div class="gallery-image-wrapper">
                        <div class="image">
                            <img src="{{ asset('DSC04115.JPG') }}" alt="">
                            <div class="opacity"><a data-fancybox="project" href="{{ asset('DSC04115.JPG') }}"
                                                    class="zoom-view" title="We’ve done lot’s of work, Let’s Check"><i
                                            class="fa fa-search" aria-hidden="true"></i></a></div>
                        </div>
                    </div> <!-- /.gallery-image-wrapper -->
                </div> <!-- /.col- -->
                <div class="col-md-3 col-xs-6">
                    <div class="gallery-image-wrapper">
                        <div class="image">
                            <img src="{{ asset('mmttc.jpg') }}" alt="">
                            <div class="opacity"><a data-fancybox="project" href="{{ asset('mmttc.jpg') }}"
                                                    class="zoom-view" title="We’ve done lot’s of work, Let’s Check"><i
                                            class="fa fa-search" aria-hidden="true"></i></a></div>
                        </div>
                    </div> <!-- /.gallery-image-wrapper -->
                </div> <!-- /.col- -->
            </div> <!-- /.row -->
        </div> <!-- /.gallery-wrapper -->
        <div class="text-center show-more"><a href="course-v1.html"
                                              class="wow fadeInUp  theme-solid-button theme-button animated"
                                              style="visibility: visible; animation-name: fadeInUp;">Our Gallery</a>
        </div>
    </div>

    <div class="course-request-section section-margin-top">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="course-request-text">
                        <div class="top-title">
                            <h3>The Best Courses</h3>
                            <h2>Knowledge is power</h2>
                            <p>Snappy High Performance Best courses behavior change and accelerates education results
                                The High Performance SellingTM solution addresses the tougher and more competitive
                                course environment created by customers demanding more value, speed and control of the
                                process.</p>
                        </div> <!-- /.top-title -->
                        <div class="course-feature-list">
                            <ul>
                                <li>
                                    <h5><i class="flaticon-exam"></i> Quality Lectures</h5>
                                    <p>We offer quality lecture rooms, content and using new technologies to ensure that
                                        you
                                        understand all the concepts therefore preparing you for the future.</p>
                                </li>
                                <li>
                                    <h5><i class="flaticon-hat"></i>Accommodation</h5>
                                    <p>We provide accomodation to our students. Our hostels are well constructed and put
                                        the students welfare and security first. Futher, we offer complimentary services
                                        such as
                                        lunch, supper and WIFI.</p>
                                </li>
                            </ul>
                            <div class="free-course-banner">
                                {{--                                <h3>Free Computer Course</h3>--}}
                                <p>Get free computer classes with any Diploma and Certificate or artisan course</p>
                                <a href="">Apply Now <i class="fa fa-arrow-right"
                                                        aria-hidden="true"></i></a>
                            </div> <!-- /.free-course-banner -->
                        </div> <!-- /.course-feature-list -->
                    </div> <!-- /.course-request-text -->
                </div> <!-- /.col- -->
                <div class="col-md-6 col-xs-12">
                    <div class="theme-form-style-one">
                        <h3>Request for Information</h3>
                        <form action="#">
                            <div class="single-input">
                                <label>Your full name</label>
                                <input type="text" placeholder="Type name">
                            </div> <!-- /.single-input -->
                            <div class="single-input">
                                <label>Your email address</label>
                                <input type="email" placeholder="Type email">
                            </div> <!-- /.single-input -->
                            <div class="single-input">
                                <label>Which Category</label>
                                <select class="selectpicker">
                                    <option>Diploma</option>
                                    <option>Certificate</option>
                                    <option>Technical</option>
                                </select>
                            </div> <!-- /.single-input -->
                            <div class="single-input">
                                <label>Short your personal message</label>
                                <textarea placeholder="Type email"></textarea>
                            </div> <!-- /.single-input -->
                            <input type="submit" value="Request Information" class="theme-solid-button theme-button">
                        </form>
                    </div> <!-- /.theme-form-style-one -->
                </div> <!-- /.col- -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div>
    <div class="blog-grid section-margin-top">
        <div class="container">
            <div class="theme-title-one theme-title">
                <h2>Our News</h2>
                <div class="icon"><i class="flaticon-book2"></i></div>
                <p>Here you can find MMTTC's news</p>
            </div> <!-- /.theme-title-one -->

            <div class="row">
                <div class="col-md-4 col-xs-6">
                    <div class="single-blog-grid hover-effect-one">
                        <div class="image">
                            <img src="{{ asset('front/images/blog/6.jpg') }}" alt="">
                            <div class="title">
                                <h5><a href="">Opening Dates</a></h5>
                                <div class="number">06</div>
                            </div>
                        </div> <!-- /.image -->
                        <div class="text">
                            <h5>Opening Dates
                            </h5>
                            <p>The opening dates are 29<sup>th</sup>, April 2019</p>
                        </div> <!-- /.text -->
                    </div> <!-- /.single-blog-grid -->
                </div>
                <div class="col-md-4 col-xs-6">
                    <div class="single-blog-grid hover-effect-one">
                        <div class="image">
                            <img src="{{ asset('front/images/blog/6.jpg') }}" alt="">
                            <div class="title">
                                <h5><a href="">Opening Dates</a></h5>
                                <div class="number">06</div>
                            </div>
                        </div> <!-- /.image -->
                        <div class="text">
                            <h5>NITA EXAMS</h5>
                            <p>NITA exams to start on 15 <sup>th</sup>, April 2019. </p>
                        </div> <!-- /.text -->
                    </div> <!-- /.single-blog-grid -->
                </div>
                <div class="col-md-4 col-xs-6">
                    <div class="single-blog-grid hover-effect-one">
                        <div class="image">
                            <img src="{{ asset('front/images/blog/6.jpg') }}" alt="">
                            <div class="title">
                                <h5><a href="">KNEC Exams</a></h5>
                                <div class="number">06</div>
                            </div>
                        </div> <!-- /.image -->
                        <div class="text">
                            <h5>KNEC Exams</h5>
                            <p>Registration for November Series. <span>Fee collection ongoing</span></p>
                        </div> <!-- /.text -->
                    </div> <!-- /.single-blog-grid -->
                </div> <!-- /.col- -->
            </div>
            <!-- /.row -->
        </div> <!-- /.container -->
    </div>
    <div class="newsletter-banner">
        <div class="container">
            <h5 class="float-left">Sign up for Subscribe Newsletter</h5>
            <form action="#" class="float-right">
                <input type="email" placeholder="Your Email">
                <button class="theme-solid-button theme-button">Submit</button>
            </form>
        </div> <!-- /.container -->
    </div>
    <div class="blog-grid section-margin-top">
        <div class="container">
            <div class="theme-title-one theme-title">
                <h2>Upcoming Events</h2>
                {{--                <div class="icon"><i class="flaticon-book2"></i></div>--}}
                {{--                <p>Here you can find MMTTC's news</p>--}}
            </div> <!-- /.theme-title-one -->

            <div class="row">
                <div class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="single-blog-grid hover-effect-one">
                            <div class="image">
                                <img src="{{ asset('banner/DSC04116.JPG') }}" alt="">
                                <div class="title">
                                    <h5><a href="">Lukenya Medical Clinic</a></h5>
                                    <div class="number">06</div>
                                </div>
                            </div> <!-- /.image -->
                            <div class="text">
                                <h5>Lukenya Medical Clinic</h5>
                                <p>Opening of Lukenya Medical Clinic.</p>
                            </div> <!-- /.text -->
                        </div> <!-- /.single-blog-grid -->
                    </div>
                    <div class="item">
                        <div class="single-blog-grid hover-effect-one">
                            <div class="image">
                                <img src="{{ asset('banner/DSC_1650.JPG') }}" alt="">
                                <div class="title">
                                    <h5><a href="">Lukenya Medical Clinic</a></h5>
                                    <div class="number">06</div>
                                </div>
                            </div> <!-- /.image -->
                            <div class="text">
                                <h5>Lukenya Plains Academy</h5>
                                <p>Opening of Lukenya Plains Academy.</p>
                            </div> <!-- /.text -->
                        </div> <!-- /.single-blog-grid -->
                    </div>
                    <div class="item">
                        <div class="single-blog-grid hover-effect-one">
                            <div class="image">
                                <img src="{{ asset('banner/DSC04961.JPG') }}" alt="">
                                <div class="title">
                                    <h5><a href="">MMTTC Choir</a></h5>
                                    <div class="number">06</div>
                                </div>
                            </div> <!-- /.image -->
                            <div class="text">
                                <h5>MMTTC Choir</h5>
                                <p>Volume III recording</p>
                            </div> <!-- /.text -->
                        </div> <!-- /.single-blog-grid -->
                    </div>
                </div>
                <!-- /.col- -->
            </div>
            <!-- /.row -->
        </div> <!-- /.container -->
    </div>
    <hr>
    <div style="background-color: #ffffff;padding: 10px;border-radius: 5px 0 5px 0">

        <div class="our-partners">
            <img src="{{ asset('new_logo_knec-2.png') }}" alt="" class="item">
            <img src="{{ asset('NITA-Logo.png') }}" alt="" class="item">
            <img src="{{ asset('kings.jpg') }}" height="153" class="item">
        </div>
    </div>
@endsection