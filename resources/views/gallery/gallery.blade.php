@extends('layouts.home')

@section('content')

    <div class="theme-inner-banner">
        <div class="opacity">
            <ul>
                <li><a href="/">Home</a></li>
                <li>/</li>
                <li>Gallery</li>
            </ul>
            <h2>Gallery</h2>
        </div> <!-- /.opacity -->
    </div> <!-- /.theme-inner-banner -->
    <div class="our-gallery section-margin-top section-margin-bottom">
        <div class="gallery-wrapper container">
            <div class="row">

            </div> <!-- /.row -->
        </div> <!-- /.gallery-wrapper -->
    </div>
    <!-- /.our-courses -->

@endsection