@extends('layouts.app')

@section('content')

    <!-- Dashboard -->
    <div class="dashboard-container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <programmes :courses='@json(\App\Programmes::all())'></programmes>
{{--                    <programmereg></programmereg>--}}
                </div>
            </div>
        </div>
    </div>

    <!-- End Dashboard -->
@endsection