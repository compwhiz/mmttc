@extends('layouts.home')

@section('content')

    <div class="theme-inner-banner">
        <div class="opacity">
            <ul>
                <li><a href="">Home</a></li>
                <li>/</li>
                <li>Business</li>
            </ul>
            <h2>Business</h2>
        </div> <!-- /.opacity -->
    </div>
    <div class="our-history section-margin-top">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img src="{{asset('director.JPG')}}" alt="">
                </div>
                <div class="col-md-8">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem harum mollitia nihil quisquam
                        vel. Ad asperiores, culpa cumque deserunt nisi non, odit officiis omnis praesentium quo ratione
                        repellendus, sed veniam?</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus at distinctio error, facere
                        iste minima molestias natus neque nulla possimus quas quia quo ratione rem soluta tempore ullam,
                        veniam voluptas.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut debitis dolorum eligendi laudantium
                        nam quis, tenetur voluptates voluptatum? Ab consequuntur ea earum eius explicabo illum incidunt
                        laboriosam, maxime optio quia?</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, consequuntur, delectus enim
                        esse facere hic nobis numquam perferendis quisquam rerum saepe sunt voluptate. Atque illo in
                        nulla quasi quos rem.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis cupiditate error est eum
                        facilis fugit hic impedit maiores, nemo, nostrum nulla odit pariatur perspiciatis sequi ullam
                        velit voluptatum. Quam, voluptatibus!</p>
                    <blockquote>
                        &mdash; Kennedy Mutisya
                    </blockquote>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-center" style="padding: 30px;">Our Courses</h5>
                    <div>
                        @foreach(\App\Programmes::all()->where('department','BUSINESS') as $course)
                            <div class="col-md-6">
                                <div class="course-warp">
                                    <div class="course-img">
                                        <img src="{{ asset('banner/DSC_1297.JPG') }}" alt="Our Courses">
                                        <div class="course-price">
                                            <h6>{{ $course->code }}</h6>
                                        </div>
                                        <div class="course-teacher">
                                            <img src="{{ asset('logo.JPG') }}" alt="teacher">
                                            <h6>MMTTC</h6>
                                        </div>
                                    </div>
                                    <div class="course-text">
                                        <h5><a href="#">{{ mb_strtoupper($course->gradetype) }} &mdash; {{$course->coursename}}</a></h5>
                                        <a href="#" class="btnapply btn-dark btn-animated">Apply Now</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-center">Our Teachers</h5>
                </div>
            </div>
        </div>
    </div>


    <div class="short-banner-two bg-two">
        <div class="opacity color-two">
            <div class="container">
                <h6>We offer quality education at friedly prices</h6>
                <a href="#" class="wow fadeInLeft animated theme-solid-button">Brochure</a>
                <a href="#" class="wow fadeInRight animated theme-line-button">Fee Structure</a>
            </div>
        </div>
    </div>
@endsection