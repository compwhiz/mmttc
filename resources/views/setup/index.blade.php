@extends('layouts.app')

@section('content')

    <!-- Dashboard -->
    <div class="dashboard-container">
        <div class="row">
            <div class="col-4">
                <schoolinfo :school="{{ \App\School::all() }}"></schoolinfo>
            </div>
        </div>
    </div>

    <!-- End Dashboard -->
@endsection