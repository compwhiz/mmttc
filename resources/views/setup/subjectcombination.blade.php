@extends('layouts.app')

@section('content')

    <!-- Dashboard -->
    <div class="dashboard-container">
        <div class="row">
            <div class="col-4">
                <subjectcombination :subjects="{{\App\Subjectcombination::all()}}"></subjectcombination>
            </div>
        </div>
    </div>

    <!-- End Dashboard -->
@endsection