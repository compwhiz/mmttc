@extends('layouts.app')

@section('content')

    <!-- Dashboard -->
    <div class="dashboard-container">
        <div class="row">
            <div class="col-4">
                <subjectgroup :subjectcategories="{{\App\Subjectgroup::all()}}"></subjectgroup>
            </div>
        </div>
    </div>

    <!-- End Dashboard -->
@endsection