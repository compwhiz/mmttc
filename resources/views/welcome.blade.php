@extends('layouts.app')

@section('content')

    <!-- Dashboard -->
    <div class="dashboard-container">
        <div class="row">
            <div class="col-xl-3 col-lg-6 col-sm-6 col-xs-12">
                <div class="widget widget-seven background-success">
                    <div class="widget-body">
                        <div href="javascript: void(0);" class="widget-body-inner">
                            <h5 class="text-uppercase">Students</h5>
                            <i class="counter-icon icmn-user"></i>
                            <span class="counter-count">
                                <i class="icmn-arrow-up5"></i>{{ \App\Student::all()->count() }}
                                <span class="counter-init" data-from="0" data-to="{{ \App\Student::all()->count() }}"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- End Dashboard -->
@endsection