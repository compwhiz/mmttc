<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::domain('portal.mmttc.ac.ke')->middleware('auth')->group(function () {

    Route::view('/', 'welcome');
    Route::resource('school', 'SchoolController');


    Route::resource('subject', 'SubjectController');
    Route::resource('subjectgroup', 'SubjectgroupController');
    Route::resource('subjectcombination', 'SubjectcombinationController');

    Route::resource('students', 'StudentController');
    Route::get('applicants', function () {
        return view('application.index');
    })->name('application.confirm');
    Route::resource('programme', 'ProgrammesController');
});
Route::get('/', function () {
    return view('front');
});

Route::post('updatepassport', 'ApplicationsController@passport');
Route::post('updatekcsecert', 'ApplicationsController@updatekcsecert');
Route::get('status/{user}', 'ApplicationsController@status')->name('status');
Route::view('contact-mmttc', 'contact');
Route::view('about', 'about');
Route::view('courses', 'courses');
Route::view('downloads', 'downloads');
Route::view('admissions', 'admissions');
Route::view('about/alumni', 'alumni');
Route::view('about/directors-message', 'director');
Route::view('about/principal-message', 'principal');
Route::view('about/student-leaders', 'student');
Route::view('gallery', 'gallery.gallery');

// school routes
Route::view('schools/medical', 'schools.medical');
Route::view('schools/engineering', 'schools.engineering');
Route::view('schools/hospitality', 'schools.hospitality');
Route::view('schools/business', 'schools.business');
Route::view('schools/computer-science', 'schools.computer');
Route::view('schools/ict', 'schools.ict');
Route::view('schools/cosmetology', 'schools.cosmetology');

Auth::routes();

Route::get('install', function () {
    Artisan::call('migrate');
});


Route::resource('application', 'ApplicationsController');
